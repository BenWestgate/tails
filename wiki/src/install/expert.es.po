# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-04-05 11:23+0000\n"
"PO-Revision-Date: 2024-05-20 18:00+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Install Tails from Debian or Ubuntu using the command line and GnuPG\"]]\n"
msgstr "[[!meta title=\"Instalar desde Debian o Ubuntu usando la línea de comandos y GnuPG\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/overview\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/overview\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/download\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/download\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"doc/about/warnings\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"doc/about/warnings\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/expert\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/expert\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/overview\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/overview.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/warnings.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/warnings.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"verify-key\" class=\"step\">Verify the Tails signing key</h1>\n"
msgstr "<h1 id=\"verify-key\" class=\"step\">Verificar la clave de firma de Tails</h1>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>If you already certified the Tails signing key with your own key, you\n"
"can skip this step and start [[downloading and verifying the USB\n"
"image|expert#download]].</p>\n"
msgstr ""
"<p>Si ya verificaste la clave de firma de Tails previamente con tu propia clave,\n"
"puedes saltarte este paso y empezar a [[descargar y verificar la imagen\n"
"USB|expert#download]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#
#. type: Plain text
msgid ""
"In this step, you will download and verify the *Tails signing key* which is "
"the OpenPGP key that is used to cryptographically sign the Tails USB image."
msgstr ""
"En este paso vas a descargar y verificar la *clave de firma de Tails* que es "
"la clave OpenPGP que se usa para firmar criptográficamente la imagen USB de "
"Tails."

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To follow these instructions you need to have your own OpenPGP\n"
"key.</p>\n"
msgstr ""
"<p>Para seguir estas instrucciones necesitas tener tu propia llave\n"
"OpenPGP.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To learn how to create yourself an OpenPGP key, see\n"
"<a href=\"https://riseup.net/en/security/message-security/openpgp/gpg-keys\">Managing\n"
"OpenPGP Keys</a> by <em>Riseup</em>.</p>\n"
msgstr ""
"<p>Para aprender a crear tu propia llave OpenPGP, consulta\n"
"<a href=\"https://riseup.net/es/security/message-security/openpgp/gpg-keys\">Managing\n"
"OpenPGP Keys</a> por <em>Riseup</em>.</p>\n"

#
#. type: Plain text
msgid ""
"This verification technique uses the OpenPGP Web of Trust and the "
"certification made by official Debian developers on the Tails signing key."
msgstr ""
"Esta técnica de verificación utiliza la Red de Confianza de OpenPGP y la "
"certificación hecha por desarrolladores oficiales de Debian en la clave de "
"firma de Tails."

#
#. type: Bullet: '1. '
msgid ""
"Import the Tails signing key in your <span class=\"application\">GnuPG</"
"span> keyring:"
msgstr ""
"Importa la clave de firma de Tails en el depósito de claves de <span "
"class=\"application\">GnuPG</span>:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">wget https://tails.net/tails-signing.key</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">gpg --import < tails-signing.key</p>\n"
msgstr ""

#
#. type: Bullet: '1. '
msgid ""
"Install the Debian keyring. It contains the OpenPGP keys of all Debian "
"developers:"
msgstr ""
"Instala el depósito de claves de Debian. Contiene las llaves OpenPGP de "
"todos los desarrolladores de Debian:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">sudo apt update && sudo apt install debian-keyring</p>\n"
msgstr "   <p class=\"command\">sudo apt update && sudo apt install debian-keyring</p>\n"

#
#. type: Bullet: '1. '
msgid ""
"Import the OpenPGP key of [[!wikipedia Chris_Lamb_(software_developer) "
"desc=\"Chris Lamb\"]], a former Debian Project Leader, from the Debian "
"keyring into your keyring:"
msgstr ""
"Importa la clave OpenPGP de [[!wikipedia Chris_Lamb_(software_developer) "
"desc=\"Chris Lamb\"]], un antiguo líder del proyecto Debian, desde el "
"depósito de claves de Debian a tu depósito de claves:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export chris@chris-lamb.co.uk | gpg --import</p>\n"
msgstr "   <p class=\"command\">gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export chris@chris-lamb.co.uk | gpg --import</p>\n"

#
#. type: Bullet: '1. '
msgid "Verify the certifications made on the Tails signing key:"
msgstr ""
"Verifica las certificaciones realizadas sobre la clave de firma de Tails:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">gpg --keyid-format 0xlong --check-sigs A490D0F4D311A4153E2BB7CADBB802B258ACD84F</p>\n"
msgstr "   <p class=\"command\">gpg --keyid-format 0xlong --check-sigs A490D0F4D311A4153E2BB7CADBB802B258ACD84F</p>\n"

#. type: Plain text
#, no-wrap
msgid "   In the output of this command, look for the following line:\n"
msgstr "   En el resultado de este comando, busca la siguiente línea:\n"

#. type: Plain text
#, no-wrap
msgid "       sig!2        0x1E953E27D4311E58 2020-03-19  Chris Lamb <chris@chris-lamb.co.uk>\n"
msgstr "       sig!2        0x1E953E27D4311E58 2020-03-19  Chris Lamb <chris@chris-lamb.co.uk>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   Here, `sig!2` means that Chris\n"
"   Lamb verified and certified the Tails signing key with his key and a level 2 check.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   It is also possible to verify the certifications made by other\n"
"   people. Their name and email address appear in the list of\n"
"   certification if you have their key in your keyring.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"caution\">\n"
"   <p>If the verification of the certification failed, then you might\n"
"   have downloaded a malicious version of the Tails signing key or our\n"
"   instructions might be outdated.\n"
"   Please [[get in touch with us|support/talk]].</p>\n"
"   </div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"tip\">\n"
"   <p>The line <code>175 signatures not checked due to missing keys</code> or similar\n"
"   refers to the certifications (also called <i>signatures</i>) made by other public\n"
"   keys that are not in your keyring. This is not a problem.</p>\n"
"   </div>\n"
msgstr ""

#
#. type: Bullet: '1. '
msgid "Certify the Tails signing key with your own key:"
msgstr "Certifica la clave de firma de Tails con tu propia clave:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">gpg --lsign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F</p>\n"
msgstr "   <p class=\"command\">gpg --lsign-key A490D0F4D311A4153E2BB7CADBB802B258ACD84F</p>\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"download\" class=\"step\">Download Tails</h1>\n"
msgstr "<h1 id=\"download\" class=\"step\">Descargar Tails</h1>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img inc/infography/download.png link=\"no\" alt=\"\"]]</div>\n"
msgstr "<div class=\"step-image\">[[!img inc/infography/download.png link=\"no\" alt=\"\"]]</div>\n"

#
#. type: Bullet: '1. '
msgid "Download the USB image:"
msgstr "Descargar la imagen USB:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">wget --continue [[!inline pages=\"inc/stable_amd64_img_url\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr ""
"   <p class=\"command\">wget --continue [[!inline pages=\"inc/"
"stable_amd64_img_url\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"verify\" class=\"step\">Verify your download</h1>\n"
msgstr "<h1 id=\"verify\" class=\"step\">Verifica tu descarga</h1>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img inc/infography/verify.png link=\"no\" alt=\"\"]]</div>\n"
msgstr "<div class=\"step-image\">[[!img inc/infography/verify.png link=\"no\" alt=\"\"]]</div>\n"

#
#. type: Plain text
msgid ""
"In this step, you will verify your download using the Tails signing key."
msgstr ""
"En este paso, verificarás tu descarga utilizando la clave de firma de Tails."

#
#. type: Bullet: '1. '
msgid "Download the signature of the USB image:"
msgstr "Descarga la firma de la imagen USB:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">wget [[!inline pages=\"inc/stable_amd64_img_sig_url\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"command\">wget [[!inline pages=\"inc/stable_amd64_img_sig_url\" raw=\"yes\" sort=\"age\"]]</p>\n"

#
#. type: Bullet: '1. '
msgid "Verify that the USB image is signed by the Tails signing key:"
msgstr ""
"Comprueba que la imagen USB está firmada por la clave de firma de Tails:"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgid "   <p class=\"command\">[[!inline pages=\"inc/stable_amd64_img_gpg_verify\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Plain text
#, no-wrap
msgid "   The output of this command should be the following:\n"
msgstr "   La salida de este comando debería ser la siguiente:\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgid "   <p class=\"code\">[[!inline pages=\"inc/stable_amd64_img_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"
msgstr "   <p class=\"pre\">[[!inline pages=\"inc/stable_amd64_img_gpg_signature_output\" raw=\"yes\" sort=\"age\"]]</p>\n"

#. type: Plain text
#, no-wrap
msgid "   Verify in this output that:\n"
msgstr "   Verifica en esta salida que:\n"

#
#. type: Bullet: '   - '
msgid "The date of the signature is the same."
msgstr "La fecha de la firma es la misma."

#. type: Bullet: '   - '
msgid ""
"The signature is marked as `Good signature` since you certified the Tails "
"signing key with your own key."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<h1 id=\"download\" class=\"step\">Download Tails</h1>\n"
msgid "<h1 id=\"install\" class=\"step\">Install Tails using <code>dd</code></h1>\n"
msgstr "<h1 id=\"download\" class=\"step\">Descargar Tails</h1>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img inc/infography/install-tails.png link=\"no\" alt=\"\"]]</div>\n"
msgstr "<div class=\"step-image\">[[!img inc/infography/install-tails.png link=\"no\" alt=\"\"]]</div>\n"

#
#. type: Bullet: '1. '
msgid ""
"Make sure that the USB stick on which you want to install Tails is unplugged."
msgstr ""

#
#. type: Bullet: '1. '
msgid "Execute the following command:"
msgstr "Ejecuta el siguiente comando:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command\">ls -1 /dev/sd?</p>\n"
msgstr "   <p class=\"command\">ls -1 /dev/sd?</p>\n"

#. type: Plain text
#, no-wrap
msgid "   It returns a list of the storage devices on the system. For example:\n"
msgstr "   Devuelve una lista de los dispositivos de almacenamiento del sistema. Por ejemplo:\n"

#. type: Plain text
#, no-wrap
msgid "       dev/sda\n"
msgstr ""

#
#. type: Bullet: '1. '
msgid "Plug in the USB stick on which you want to install Tails."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <div class=\"caution\"><p>All the data on this USB stick will be lost.</p></div>\n"
msgstr "   <div class=\"caution\"><p>Se perderá toda la información en esta memoria USB.</p></div>\n"

#
#. type: Bullet: '1. '
msgid "Execute again the same command:"
msgstr "Ejecuta otra vez el mismo comando:"

#. type: Plain text
#, no-wrap
msgid "   Your USB stick appears as a new device in the list.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"       /dev/sda\n"
"       /dev/sdb\n"
msgstr ""

#
#. type: Bullet: '1. '
msgid "Take note of the *device name* of your USB stick."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   In this example, the device name of the USB stick is\n"
"   `/dev/sdb`. Yours might be different.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"caution\">\n"
"   <p>If you are unsure about the device name, you should stop proceeding or\n"
"   <strong>you risk overwriting any hard disk on the system</strong>.</p>\n"
"   </div>\n"
msgstr ""
"   <div class=\"caution\">\n"
"<p>Si no estás seguro del nombre del dispositivo deberías parar ahora o\n"
"<strong>te arriesgas a sobrescribir cualquier otro disco duro del sistema</strong>.</p>\n"
"</div>\n"

#
#. type: Bullet: '1. '
msgid ""
"Execute the following commands to copy the USB image that you downloaded "
"earlier to the USB stick."
msgstr ""
"Ejecuta los siguientes comandos para copiar la imagen USB que descargaste "
"previamente a la memoria USB."

#. type: Plain text
#, no-wrap
msgid "   Replace:\n"
msgstr "   Reemplaza:\n"

#
#. type: Bullet: '   - '
msgid ""
"<span class=\"command-placeholder\">tails.img</span> with the path to the "
"USB image"
msgstr ""
"<span class=\"command-placeholder\">tails.img</span> con la ruta de la "
"imagen USB"

#
#. type: Bullet: '   - '
msgid ""
"<span class=\"command-placeholder\">device</span> with the device name found "
"in step 5"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command-template\">dd if=<span class=\"command-placeholder\">tails.img</span> of=<span class=\"command-placeholder\">device</span> bs=16M oflag=direct status=progress</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   You should get something like this:\n"
msgstr "   Deberías obtener algo como esto:\n"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command-template\">dd if=/home/user/tails-amd64-3.12.img of=/dev/sdb bs=16M oflag=direct status=progress</p>\n"
msgstr ""
"   <p class=\"command-template\">dd if=/home/user/tails-amd64-3.12.img of=/"
"dev/sdb bs=16M oflag=direct status=progress</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   If no error message is returned, Tails is being copied on the USB\n"
"   stick. The copy takes some time, generally a few minutes.\n"
msgstr ""
"   Si no aparece ningún mensaje de error, Tails está siendo copiado a la memoria\n"
"   USB. El proceso de copiado tarda por lo general un par de minutos.\n"

#. type: Plain text
#, no-wrap
msgid "   <div class=\"note\">\n"
msgstr "   <div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <p>If you get a <code>Permission denied</code> error, try\n"
"   adding <code>sudo</code> at the beginning of the command:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command-example\">sudo dd if=<span class=\"command-placeholder\">tails.img</span> of=<span class=\"command-placeholder\">device</span> bs=16M oflag=direct status=progress</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   </div>\n"
msgstr "   </div>\n"

#. type: Plain text
#, no-wrap
msgid "   The installation is complete after the command prompt reappears.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_first_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/restart_first_time.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/welcome.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/welcome.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#, no-wrap
#~ msgid ""
#~ "       wget https://tails.net/tails-signing.key\n"
#~ "       gpg --import < tails-signing.key\n"
#~ msgstr ""
#~ "       wget https://tails.net/tails-signing.key\n"
#~ "       gpg --import < tails-signing.key\n"

#, no-wrap
#~ msgid "   <p class=\"pre command-output\">/dev/sda</p>\n"
#~ msgstr "   <p class=\"pre command-output\">/dev/sda</p>\n"

#, no-wrap
#~ msgid ""
#~ "   <p class=\"pre command-output\">/dev/sda\n"
#~ "   /dev/sdb</p>\n"
#~ msgstr ""
#~ "   <p class=\"pre command-output\">/dev/sda\n"
#~ "   /dev/sdb</p>\n"
