# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-05-27 21:53+0000\n"
"PO-Revision-Date: 2024-05-06 09:51+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"requirements/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"System requirements and recommended hardware\"]]\n"
msgstr "[[!meta title=\"Requisitos del sistema y hardware recomendado\"]]\n"

#. type: Title =
#, no-wrap
msgid "Summary"
msgstr "Resumen"

#. type: Plain text
#, fuzzy
#| msgid "- Tails might not work on:"
msgid "- Tails works on:"
msgstr "- Tails podría no funcionar en:"

#. type: Bullet: '  * '
#, fuzzy
#| msgid "- Tails works on most PC computers that are less than 10 years old."
msgid "Most PC computers that are less than 10 years old"
msgstr ""
"- Tails funciona en la mayoría de las computadoras PC con menos de 10 años "
"de antigüedad."

#. type: Bullet: '  * '
#, fuzzy
#| msgid "Tails works on some older Mac computers with an Intel processor."
msgid "Some older Mac computers with an Intel processor"
msgstr ""
"Tails funciona en algunas computadoras Mac antiguas con procesador Intel."

#. type: Plain text
#, fuzzy
#| msgid "- Tails might not work on:"
msgid "- Tails does not work:"
msgstr "- Tails podría no funcionar en:"

#. type: Bullet: '  * '
#, fuzzy
#| msgid ""
#| "- Tails does not work on newer Mac computers with an Apple processor (M1 "
#| "or M2)."
msgid "Newer Mac computers with an Apple processor (M1 or M2)"
msgstr ""
"- Tails no funciona con modelos de Mac con un procesador de Apple (M1 o M2)."

#. type: Bullet: '  * '
msgid "Smartphones or tablets"
msgstr ""

#. type: Bullet: '  * '
msgid "Raspberry Pi"
msgstr "Raspberry Pi"

#. type: Plain text
msgid "- Tails might not work on:"
msgstr "- Tails podría no funcionar en:"

#. type: Bullet: '  * '
#, fuzzy
#| msgid "Some older computers, for example, if they don't have enough RAM."
msgid "Some older computers, for example, if they don't have 2 GB of RAM."
msgstr ""
"Algunas computadoras más antiguas, por ejemplo, si no tienen suficiente RAM."

#. type: Bullet: '  * '
#, fuzzy
#| msgid ""
#| "Some newer computers, for example, if their [[graphics card is "
#| "incompatible with Linux|support/known_issues/graphics]]."
msgid ""
"Some newer computers, for example, if their [[graphics card is incompatible "
"with Linux|support/known_issues/graphics]].  Nvidia or AMD Radeon cards "
"often do not work in Tails."
msgstr ""
"Algunas computadoras nuevas, por ejemplo, si su [[tarjeta gráfica es "
"incompatible con Linux|support/known_issues/graphics]]."

#. type: Plain text
#, no-wrap
msgid ""
"<p>See our [[list of known hardware compatibility\n"
"issues|support/known_issues]].</p>\n"
msgstr ""
"<p>Mira nuestra [[lista de problemas conocidos de compatibilidad\n"
"de hardware|support/known_issues]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Title =
#, fuzzy, no-wrap
#| msgid "Hardware requirements"
msgid "Detailed hardware requirements"
msgstr "Requerimientos de Hardware"

#. type: Plain text
msgid "- A USB stick of 8 GB minimum or a recordable DVD."
msgstr "- Una memoria USB de al menos 8 GB o un DVD grabable."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "  All the data on this USB stick or DVD will be lost when installing Tails.\n"
msgid "  All the data on this USB stick or DVD is lost when installing Tails.\n"
msgstr "  Toda la información en la memoria USB o DVD se perderá cuando se instale Tails.\n"

#. type: Plain text
msgid "- The ability to start from a USB stick or a DVD reader."
msgstr ""
"- La posibilidad de iniciar desde una memoria USB o una lectora de DVD."

#. type: Bullet: '- '
#, fuzzy
#| msgid ""
#| "A 64-bit <span class=\"definition\">[[!wikipedia x86-64]]</span> "
#| "compatible processor:"
msgid ""
"A 64-bit [[!wikipedia x86-64]] [[!wikipedia IBM PC compatible]] processor."
msgstr ""
"Un procesador de 64-bits <span class=\"definition\">[[!wikipedia_es "
"x86-64]]</span> compatible:"

#. type: Bullet: '  * '
msgid ""
"Tails does not works on [[!wikipedia ARM_architecture desc=\"ARM\"]] or [[!"
"wikipedia PowerPC]] processors."
msgstr ""

#. type: Bullet: '  * '
msgid ""
"Tails does not work on 32-bit computers since [[Tails 3.0|news/"
"Tails_3.0_will_require_a_64-bit_processor]] (June 2017)."
msgstr ""
"Tails no funciona en computadoras de 32 bits desde [[Tails 3.0|news/"
"Tails_3.0_will_require_a_64-bit_processor]] (Junio de 2017)."

#. type: Plain text
msgid "- 2 GB of RAM to work smoothly."
msgstr "- 2 GB de memoria RAM para trabajar cómodamente."

#. type: Plain text
#, no-wrap
msgid "  Tails can work with less than 2 GB RAM but might behave strangely or crash.\n"
msgstr "  Tails puede funcionar con menos de 2 GB de memoria RAM pero puede comportarse de manera extraña o fallar.\n"

#. type: Title =
#, no-wrap
msgid "Recommended hardware"
msgstr "Hardware recomendado"

#. type: Plain text
msgid ""
"Laptop models evolve too rapidly for us to be able to provide an up-to-date "
"list of recommended hardware. Below are some guidelines if you, or your "
"organization, are considering acquiring a laptop dedicated to running Tails."
msgstr ""
"Los modelos de portátiles evolucionan demasiado rápido como para que podamos "
"ofrecer una lista actualizada del hardware recomendado. A continuación te "
"ofrecemos algunas pautas si tú, o tu organización, estáis considerando "
"adquirir un portátil dedicado a ejecutar Tails."

#. type: Title ###
#, no-wrap
msgid "For PC"
msgstr "Para PC"

#. type: Plain text
msgid "- Avoid \"gaming\" models with Nvidia or AMD Radeon graphics cards."
msgstr ""
"- Evita los modelos \"gaming\" con tarjetas gráficas Nvidia o AMD Radeon."

#. type: Bullet: '- '
msgid ""
"Consider buying a refurbished laptop from a high-end (professional) series.  "
"These are cheaper and will last longer than new but lower-quality laptops."
msgstr ""
"Considera comprar un portátil reacondicionado de una serie de alta gama "
"(profesional). Estos son mas baratos y durarán mas que un portátil nuevo y "
"de menor calidad."

#. type: Bullet: '  * '
msgid ""
"For example, the Lenovo ThinkPad series work well with Tails, including the "
"X250, X1 Carbon, T440, T480, and T490 models."
msgstr ""
"Por ejemplo, la serie ThinkPad de Lenovo funciona bien con Tails, incluyendo "
"los modelos X250, X1 Carbon, T440, y T490."

#. type: Bullet: '  * '
msgid ""
"If you live in a part of the world where buying refurbished laptops is "
"uncommon, look on eBay and Amazon. Amazon offers a 90-day [Amazon Renewed "
"Guarantee](https://www.amazon.com/gp/help/customer/display.html?"
"nodeId=G4ZAA22U35N373NX)."
msgstr ""
"Si vives en una parte del mundo donde los portátiles reacondicionados no son "
"comunes, busca en eBay o en Amazon. Amazon ofrece una [Garantía de Amazon "
"Renewed](https://www.amazon.es/gp/help/customer/display.html?"
"nodeId=GJB4W4URK6H2E6Y9) de 90 días."

#. type: Bullet: '- '
msgid ""
"Consider buying a new laptop from vendors who guarantee the compatibility "
"with Linux and Tails like [ThinkPenguin](https://www.thinkpenguin.com/)."
msgstr ""
"Considera comprar una portátil nueva de vendedores que garanticen "
"compatibilidad con Linux y Tails, como [ThinkPenguin](https://www."
"thinkpenguin.com/)."

#. type: Title ###
#, no-wrap
msgid "For Mac"
msgstr "Para Mac"

#. type: Plain text
msgid ""
"Unfortunately, we don't know of any Mac model that works well in Tails and "
"can run the latest macOS version."
msgstr ""
"Desafortunadamente, no conocemos ningún modelo de Mac que funcione bien con "
"Tails y pueda ejecutar la última versión de macOS."

#. type: Plain text
#, no-wrap
msgid "<!--\n"
msgstr "<!--\n"

#. type: Plain text
msgid "To update the list of Mac computer:"
msgstr "To update the list of Mac computer:"

#. type: Bullet: '1. '
msgid ""
"Store an archive of WhisperBack reports from the last 6 months in a folder."
msgstr ""
"Store an archive of WhisperBack reports from the last 6 months in a folder."

#. type: Bullet: '2. '
msgid "Decrypt all the reports:"
msgstr "Decrypt all the reports:"

#. type: Plain text
#, no-wrap
msgid "   ~/Tails/blueprints/stats/whisperback_scripts/decrypt.rb\n"
msgstr "   ~/Tails/blueprints/stats/whisperback_scripts/decrypt.rb\n"

#. type: Bullet: '3. '
msgid "Extract the list of computer models:"
msgstr "Extract the list of computer models:"

#. type: Plain text
#, no-wrap
msgid "   ~/Tails/blueprints/stats/whisperback_scripts/content_of.rb \"/usr/sbin/dmidecode -s system-product-name\" > machines\n"
msgstr "   ~/Tails/blueprints/stats/whisperback_scripts/content_of.rb \"/usr/sbin/dmidecode -s system-product-name\" > machines\n"

#. type: Bullet: '4. '
msgid "Sort and count identical models:"
msgstr "Sort and count identical models:"

#. type: Plain text
#, no-wrap
msgid "   grep -v Bug_report machines | sort | uniq -c | sort -rhk 1 > top\n"
msgstr "   grep -v Bug_report machines | sort | uniq -c | sort -rhk 1 > top\n"

#. type: Bullet: '5. '
msgid ""
"Share WhisperBack reports number with the Foundations team and ask them to "
"evaluate hardware compatibility hints in those reports."
msgstr ""
"Share WhisperBack reports number with the Foundations team and ask them to "
"evaluate hardware compatibility hints in those reports."

#. type: Plain text
#, no-wrap
msgid "   While analyzing the reports corresponding from each model, FT should:\n"
msgstr "   While analyzing the reports corresponding from each model, FT should:\n"

#. type: Bullet: '   - '
msgid "Check whether they were sent from the same email address, if any"
msgstr "Check whether they were sent from the same email address, if any"

#. type: Bullet: '   - '
msgid "Check whether the wlan0 interface was the same hardware device"
msgstr "Check whether the wlan0 interface was the same hardware device"

#. type: Bullet: '   - '
msgid "Check whether the report was about hardware compatibility issues"
msgstr "Check whether the report was about hardware compatibility issues"

#. type: Bullet: '6. '
msgid ""
"Check whether these Mac models still support the latest version of macOS."
msgstr ""
"Check whether these Mac models still support the latest version of macOS."

#. type: Plain text
msgid "To update the list of PC computers:"
msgstr "To update the list of PC computers:"

#. type: Plain text
msgid "- Check what's commonly available with refurbishing companies."
msgstr "- Check what's commonly available with refurbishing companies."

#. type: Plain text
msgid "- Ask assembly@ for models."
msgstr "- Ask assembly@ for models."

#. type: Plain text
#, no-wrap
msgid "-->\n"
msgstr "-->\n"

#, fuzzy
#~| msgid "Tails works on most computers less than 10 years old."
#~ msgid "- Tails works on some older Mac computers with an Intel processor."
#~ msgstr ""
#~ "Tails funciona en la mayoría de las computadoras con menos de 10 años de "
#~ "antigüedad."

#~ msgid ""
#~ "\"Gaming\" graphics cards like Nvidia or AMD Radeon, which are often "
#~ "incompatible."
#~ msgstr ""
#~ "Tarjetas gráficas para \"juegos\" como Nvidia o AMD Radeon, que suelen "
#~ "ser incompatibles."

#~ msgid ""
#~ "<span class=\"definition\">[[!wikipedia IBM_PC_compatible]]</span> but "
#~ "not <span class=\"definition\">[[!wikipedia PowerPC]]</span> nor <span "
#~ "class=\"definition\">[[!wikipedia ARM_architecture desc=\"ARM\"]]</span>."
#~ msgstr ""
#~ "<span class=\"definition\">[[!wikipedia_es Compatible_IBM_PC]]</span> "
#~ "pero no <span class=\"definition\">[[!wikipedia_es PowerPC]]</span> o "
#~ "<span class=\"definition\">[[!wikipedia_es Arquitectura_ARM "
#~ "desc=\"ARM\"]]</span>."

#~ msgid ""
#~ "Tails does not work on newer Mac computers with an [[!wikipedia "
#~ "Apple_silicon#M_series desc=\"Apple processor\"]] (M1 or M2)."
#~ msgstr ""
#~ "Tails no funciona en las nuevas computadoras Mac con un [[!wikipedia_es "
#~ "Apple_Silicon#Serie_M desc=\"procesador Apple\"]] (M1 o M2)."

#~ msgid "Tails does not work on phone or tablets."
#~ msgstr "Tails no funciona en teléfonos ni tablets."

#, no-wrap
#~ msgid "<div class=\"note\">\n"
#~ msgstr "<div class=\"note\">\n"

#~ msgid "Most Mac computers are IBM PC compatible since 2006."
#~ msgstr ""
#~ "La mayoría de las computadoras Mac son compatibles con IBM PC desde 2006."

#, no-wrap
#~ msgid ""
#~ "- A 64-bit <span class=\"definition\">[[!wikipedia x86-64]]</span>\n"
#~ "  compatible processor:\n"
#~ "  - <span class=\"definition\">[[!wikipedia IBM_PC_compatible]]</span> but not\n"
#~ "    <span class=\"definition\">[[!wikipedia PowerPC]]</span> nor\n"
#~ "    <span class=\"definition\">[[!wikipedia ARM_architecture desc=\"ARM\"]]</span>.\n"
#~ "  - Most Mac computers are IBM PC compatible since 2006.\n"
#~ "  - Tails does not work with Mac models that use the [[!wikipedia Apple M1]] chip.\n"
#~ "  - Tails does not work on 32-bit computers since [[Tails 3.0|news/Tails_3.0_will_require_a_64-bit_processor]] (June 2017).\n"
#~ "  - Tails does not work on most tablets and phones.\n"
#~ msgstr ""
#~ "- Un procesador 64-bit <span class=\"definition\">[[!wikipedia_es x86-64]]</span>\n"
#~ "compatible con:\n"
#~ "- <span class=\"definition\">[[!wikipedia_es Compatible_IBM_PC]]</span>, pero no\n"
#~ "<span class=\"definition\">[[!wikipedia_es PowerPC]]</span> ni\n"
#~ "<span class=\"definition\">[[!wikipedia_es Arquitectura_ARM]]</span>.\n"
#~ "- La mayoria de las computadoras Mac son compatibles con computadoras IBM desde 2006.\n"
#~ "- Tails no funciona con modelos Mac que utilizan el [[!wikipedia Apple M1]] chip\n"
#~ "- Tails no funciona en computadoras de 32-bits desde [[Tails 3.0|news/Tails_3.0_will_require_a_64-bit_processor]] (Junio 2017).\n"
#~ "- Tails no funciona practicamente en ninguna tablet o teléfono.\n"

#~ msgid ""
#~ "Tails works on most reasonably recent computers, say manufactured after "
#~ "2008.  Here is a detailed list of requirements:"
#~ msgstr ""
#~ "Tails funciona en cualquier computador razonablemente reciente, digamos "
#~ "manufacturado después de 2008. Aquí hay una lista detallada de requisitos:"

#~ msgid ""
#~ "Either **an internal or external DVD reader** or the possibility to "
#~ "**boot from a USB stick**."
#~ msgstr ""
#~ "Ya sea **una unidad externa o interna de DVD** o la posibilidad de "
#~ "**arrancar desde un dispositivo USB**."
