# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2023-11-23 14:17+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Checking the file system of the Persistent Storage\"]]\n"
msgstr "[[!meta title=\"Das Dateisystem des beständigen Speicherbereichs überprüfen\"]]\n"

#. type: Plain text
msgid ""
"In rare occasions, you might have to perform a file system check to repair a "
"broken Persistent Storage."
msgstr ""
"In seltenen Fällen müssen Sie das Dateisystem überprüfen, um einen defekten, "
"beständigen Speicherbereich zu reparieren."

#. type: Title =
#, no-wrap
msgid "Unlock the Persistent Storage"
msgstr "Entsperren Sie den beständigen Speicherbereich"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "When starting Tails, keep the Persistent Storage locked and [[set up an "
#| "administration password|doc/first_steps/welcome_screen/"
#| "administration_password]]."
msgid ""
"When starting Tails, [[set up an administration password|doc/first_steps/"
"welcome_screen/administration_password]]."
msgstr ""
"Wenn Sie Tails starten, halten Sie den Persistent Storage gesperrt und "
"[[richten das Administrationspasswort ein|doc/first_steps/welcome_screen/"
"administration_password]]."

#. type: Bullet: '1. '
msgid ""
"Choose **Applications**&nbsp;▸ **Utilities**&nbsp;▸ **Disks** to open the "
"*Disks* utility."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"In the left pane, click on the device corresponding to your Tails USB stick."
msgstr ""
"Wählen Sie im linken Fensterbereich das Speichermedium aus, welches Ihrem "
"USB-Stick mit Tails entspricht."

#. type: Bullet: '1. '
msgid ""
"In the right pane, click on the partition labeled as <span "
"class=\"guilabel\">TailsData LUKS</span>."
msgstr ""
"Klicken Sie im rechten Fensterbereich auf die Partition mit dem Namen <span "
"class=\"guilabel\">TailsData LUKS</span>."

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "Click on the <span class=\"guimenu\">[[!img lib/unlock.png alt=\"Unlock\" "
#| "class=\"symbolic\" link=\"no\"]]</span> button to unlock the persistent "
#| "volume. Enter the passphrase of the persistent volume and click <span "
#| "class=\"guilabel\">Unlock</span>."
msgid ""
"Click on [[!img lib/network-wireless-encrypted.png alt=\"Unlock\" "
"class=\"symbolic\" link=\"no\"]]</span> to unlock the Persistent Storage. "
"Enter the passphrase of the Persistent Storage and click **Unlock** again."
msgstr ""
"Klicken Sie auf die Schaltfläche <span class=\"guimenu\">[[!img lib/unlock."
"png alt=\"Entsperren\" class=\"symbolic\" link=\"no\"]]</span>, um den "
"beständigen Speicherbereich freizuschalten. Geben Sie die Passphrase des "
"beständigen Speicherbereichs ein und klicken Sie auf <span "
"class=\"guilabel\">Entsperren</span>."

#. type: Bullet: '1. '
msgid ""
"In the confirmation dialog, enter your administration password and click "
"<span class=\"guilabel\">Authenticate</span>."
msgstr ""
"Geben Sie in dem Bestätigungsdialog Ihr Administrationspasswort ein und "
"klicken Sie auf <span class=\"guilabel\">Anmelden</span>."

#. type: Bullet: '1. '
msgid ""
"Click on the <span class=\"guilabel\">TailsData Ext4</span> partition that "
"appears below the <span class=\"guilabel\">TailsData LUKS</span> partition."
msgstr ""
"Klicken Sie auf die Partition <span class=\"guilabel\">TailsData Ext4</"
"span>, die unter der Partition <span class=\"guilabel\">TailsData LUKS</"
"span> erscheint."

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "Identify the device name of your Persistent Storage. The device name "
#| "appears below the list of volumes.  It should look like <code>/dev/mapper/"
#| "luks-xxxxxxxx</code>."
msgid ""
"Identify the device name of your Persistent Storage. The device name appears "
"below the list of volumes.  It should look like `/dev/mapper/luks-xxxxxxxx`."
msgstr ""
"Identifizieren Sie den Gerätenamen Ihres dauerhaften Speichers. Der "
"Gerätename erscheint unterhalb der Liste der Laufwerke. Er sollte wie <code>/"
"dev/mapper/luks-xxxxxxxx</code> aussehen."

#. type: Plain text
#, no-wrap
msgid ""
"   Triple-click to select it and press **Ctrl+C** to\n"
"   copy it to the clipboard.\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Check the file system using the terminal"
msgstr "Das Dateisystem im Terminal überprüfen"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "1. Choose\n"
#| "    <span class=\"menuchoice\">\n"
#| "      <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#| "      <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
#| "      <span class=\"guimenuitem\">Root Terminal</span></span>\n"
#| "   and enter your administration password to open a root terminal.\n"
msgid ""
"1. Choose\n"
"    <span class=\"menuchoice\">\n"
"      <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"      <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
"      <span class=\"guimenuitem\">Root Terminal</span></span>\n"
"   and enter your administration password to open a root terminal.\n"
msgstr ""
"1. Wählen Sie\n"
"    <span class=\"menuchoice\">\n"
"      <span class=\"guimenu\">Anwendungen</span>&nbsp;▸\n"
"      <span class=\"guisubmenu\">Systemwerkzeuge</span>&nbsp;▸\n"
"      <span class=\"guimenuitem\">Root Terminal</span></span>\n"
"   und geben Sie ihr Administrationspasswort ein, um ein Terminal mit Administrationsrechten zu starten.\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "In the terminal, execute the following command, replacing `[device]` with "
#| "the device name found in step 8:"
msgid ""
"In the terminal, execute the following command. Replace <span "
"class=\"command-placeholder\">device</span> with the device name found in "
"step 8:"
msgstr ""
"Führen Sie im Terminal folgenden Befehl aus und ersetzen Sie `[Gerät]` mit "
"dem Gerätenamen, den Sie in Schritt 8 herausgefunden haben:"

#. type: Plain text
#, no-wrap
msgid "   <p class=\"command-template\">fsck -y <span class=\"command-placeholder\">device</span></p>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "   To do so, you can type <span class=\"command\">fsck -y&nbsp;</span> and press\n"
#| "   <span class=\"keycap\">Shift+Ctrl+V</span> to paste the device name from the\n"
#| "   clipboard.\n"
msgid ""
"   To do so, you can type <code>fsck -y&nbsp;</code> and press\n"
"   **Shift+Ctrl+V** to paste the device name from the\n"
"   clipboard.\n"
msgstr ""
"   Um dies zu tun, können Sie <span class=\"command\">fsck -y&nbsp;</span> eingeben und \n"
"   <span class=\"keycap\">Shift+Strg+V</span> drücken, um den Gerätenamen aus der Zwischenablage\n"
"   einzufügen.\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "If the file system is free of errors, the last line from the output of "
#| "<span class=\"command\">fsck</span> starts with <span "
#| "class=\"command\">TailsData: clean</span>."
msgid ""
"If the file system is free of errors, the last line from the output of "
"`fsck` starts with `TailsData: clean`."
msgstr ""
"Wenn das Dateisystem fehlerfrei ist, beginnt die letzte Zeile der Ausgabe "
"von <span class=\"command\">fsck</span> mit <span "
"class=\"command\">TailsData: clean</span>."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "   If the file system has errors, <span class=\"command\">fsck</span> tries to\n"
#| "   fix them automatically. After it finishes, you can try executing the same\n"
#| "   command again to check if all errors are solved.\n"
msgid ""
"   If the file system has errors, `fsck` tries to\n"
"   fix them automatically. After it finishes, you can try executing the same\n"
"   command again to check if all errors are solved.\n"
msgstr ""
"   Falls das Dateisystem fehlerhaft ist, versucht <span class=\"command\">fsck</span> diese automatisch\n"
"   zu beheben. Nachdem es fertig ist, können Sie den Befehl erneut ausführen, um zu überprüfen ob alle\n"
"   Fehler gelöst sind.\n"

#, fuzzy, no-wrap
#~| msgid ""
#~| "1. Choose\n"
#~| "   <span class=\"menuchoice\">\n"
#~| "     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~| "     <span class=\"guisubmenu\">Utilities</span>&nbsp;▸\n"
#~| "     <span class=\"guimenuitem\">Disks</span>\n"
#~| "   </span>\n"
#~| "   to open <span class=\"application\">GNOME Disks</span>.\n"
#~ msgid ""
#~ "1. Choose\n"
#~ "   <span class=\"menuchoice\">\n"
#~ "     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~ "     <span class=\"guisubmenu\">Utilities</span>&nbsp;▸\n"
#~ "     <span class=\"guimenuitem\">Disks</span>\n"
#~ "   </span>\n"
#~ "   to open <span class=\"application\">GNOME Disks</span>.\n"
#~ msgstr ""
#~ "1. Wählen Sie\n"
#~ "   <span class=\"menuchoice\">\n"
#~ "     <span class=\"guimenu\">Anwendungen</span>&nbsp;▸\n"
#~ "     <span class=\"guisubmenu\">Hilfsprogramme</span>&nbsp;▸\n"
#~ "     <span class=\"guimenuitem\">Laufwerke</span>\n"
#~ "   </span>\n"
#~ "   um <span class=\"application\">GNOME Laufwerke</span> zu starten.\n"

#, no-wrap
#~ msgid "        fsck -y [device]\n"
#~ msgstr "        fsck -y [Gerät]\n"

#~ msgid "Click on <span class=\"guilabel\">Check Filesystem</span>."
#~ msgstr ""
#~ "Klicken Sie auf <span class=\"guilabel\">Dateisystem überprüfen</span> ."
