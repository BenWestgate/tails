# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-12-21 23:10+0000\n"
"PO-Revision-Date: 2023-12-21 23:44+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
msgid "- [[Removing metadata from files|sensitive_documents/metadata]]"
msgstr "- [[Eliminació de metadades de fitxers|sensitive_documents/metadata]]"

#. type: Plain text
msgid "- [[Office suite|sensitive_documents/office_suite]]"
msgstr "- [[Paquet ofimàtic|sensitive_documents/office_suite]]"

#. type: Plain text
msgid "- [[Graphics|sensitive_documents/graphics]]"
msgstr "- [[Gràfics|sensitive_documents/graphics]]"

#. type: Plain text
msgid "- [[Sound and video|sensitive_documents/sound_and_video]]"
msgstr "- [[So i vídeo|sensitive_documents/sound_and_video]]"

#. type: Plain text
msgid "- [[Printing and scanning|sensitive_documents/printing_and_scanning]]"
msgstr "- [[Impressió i escaneig|sensitive_documents/printing_and_scanning]]"

#. type: Plain text
msgid ""
"- [[Screenshot and screencast|sensitive_documents/screenshot_and_screencast]]"
msgstr ""
"- [[Captura i enregistrament de pantalla|sensitive_documents/"
"screenshot_and_screencast]]"
