# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-04-05 02:50+0000\n"
"PO-Revision-Date: 2024-04-05 17:36+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Why does Tails use Tor?\"]]\n"
msgstr "[[!meta title=\"Per què Tails utilitza Tor?\"]]\n"

#. type: Plain text
msgid "Tails uses Tor because it is the best available anonymity network."
msgstr "Tails utilitza Tor perquè és la millor xarxa d'anonimat disponible."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"anonymity\">Anonymity enforcement</h1>\n"
msgstr "<h1 id=\"anonymity\">Compliment de l'anonimat</h1>\n"

#. type: Plain text
msgid ""
"We want to enforce good security by default for our users. That is why it is "
"a fundamental assumption of Tails to force all outgoing traffic to anonymity "
"networks such as Tor."
msgstr ""
"Volem aplicar una bona seguretat per defecte per als nostres usuaris. Per "
"això, és un supòsit fonamental de Tails forçar tot el trànsit de sortida a "
"xarxes d'anonimat com Tor."

#. type: Plain text
msgid ""
"Over the years Tor has become a big network with a lot of capacity and a "
"good speed."
msgstr ""
"Amb els anys Tor s'ha convertit en una gran xarxa amb molta capacitat i una "
"bona velocitat."

#. type: Plain text
msgid ""
"Virtual Private Networks (VPNs) could be faster than Tor but they are not "
"anonymity networks, because the administrators of the VPN can know both "
"where you are connecting from and where you are connecting to and break your "
"anonymity. Tor provides anonymity by making it impossible for a single point "
"in the network to know both the origin and the destination of a connection."
msgstr ""
"Les xarxes privades virtuals (VPN) podrien ser més ràpides que Tor, però no "
"són xarxes d'anonimat, perquè els administradors de la VPN poden saber tant "
"des d'on us connecteu com on us connecteu i trencar el vostre anonimat. Tor "
"proporciona l'anonimat ja que fa impossible que un únic punt de la xarxa "
"conegui tant l'origen com la destinació d'una connexió."

#. type: Plain text
msgid ""
"When using a VPN, an attacker can also break your anonymity by monitoring "
"the incoming and outgoing connections of the few servers of the VPN. On the "
"other hand, the Tor network is formed by over 7500 relays run worldwide by "
"volunteers."
msgstr ""
"Quan utilitzeu una VPN, un atacant també pot trencar el vostre anonimat "
"supervisant les connexions entrants i sortints dels pocs servidors de la "
"VPN. D'altra banda, la xarxa Tor està formada per més de 7500 repetidors "
"gestionats arreu del món per voluntaris."

#. type: Plain text
#, no-wrap
msgid "<div class=\"next\">\n"
msgstr "<div class=\"next\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>See also our warnings about the [[limitations of the Tor\n"
"network|doc/about/warnings/tor]].</p>\n"
msgstr ""
"<p>Vegeu també les nostres advertències sobre les [[limitacions de la xarxa\n"
"Tor|doc/about/warnings/tor]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"popularity\">User base</h1>\n"
msgstr "<h1 id=\"popularity\">Base d'usuaris</h1>\n"

#. type: Plain text
msgid ""
"Tor is the anonymity network with the largest user base. More than "
"4&thinsp;000&thinsp;000 users connected to Tor daily in 2023. Being adopted "
"by such a large audience proves its maturity, stability, and usability."
msgstr ""
"Tor és la xarxa d'anonimat amb la base d'usuaris més gran. Més de "
"4&thinsp;000&thinsp;000 usuaris es van connectar a Tor diàriament el 2023. "
"Ser adoptat per un públic tan gran demostra la seva maduresa, estabilitat i "
"usabilitat."

#. type: Plain text
msgid ""
"Tor is being used equally by journalists, law enforcement, governments, "
"human rights activists, business leaders, militaries, abuse victims and "
"average citizens concerned about online privacy. This diversity actually "
"provides stronger anonymity to everyone as it makes it more difficult to "
"identify or target a specific profile of Tor user. Anonymity loves company."
msgstr ""
"Tor està sent utilitzat per igual per periodistes, forces de l'ordre, "
"governs, activistes dels drets humans, líders empresarials, militars, "
"víctimes d'abús i ciutadans mitjans preocupats per la privadesa en línia. En "
"realitat, aquesta diversitat proporciona un anonimat més fort per a tothom, "
"ja que fa que sigui més difícil identificar o orientar un perfil específic "
"d'usuari de Tor. L'anonimat estima la companyia."

#. type: Title =
#, no-wrap
msgid "Technical merits and recognition"
msgstr "Mèrits tècnics i reconeixement"

#. type: Plain text
msgid ""
"Tor has partnered with leading research institutions, and has been subjected "
"to intensive academic research. It is the anonymity network which benefits "
"from the most auditing and peer review."
msgstr ""
"Tor s'ha associat amb institucions de recerca líders i ha estat sotmès a una "
"intensa investigació acadèmica. És la xarxa d'anonimat que es beneficia de "
"la majoria d'auditoria i revisió per iguals."

#. type: Plain text
msgid ""
"Tor has been received awards by institutions such as the [Electronic "
"Frontier Foundation](https://www.eff.org/awards/pioneer/2012), and the [Free "
"Software Foundation](https://www.fsf.org/news/2010-free-software-awards-"
"announced) to name a few."
msgstr ""
"Tor ha rebut premis per institucions com la [Electronic Frontier Foundation]"
"(https://www.eff.org/awards/pioneer/2012) i la [Free Software Foundation]"
"(https://www.fsf.org/news/2010-free-software-awards-announced) per citar-ne "
"alguns."

#. type: Plain text
msgid ""
"An extract of a Top Secret appraisal by the NSA characterized Tor as \"[the "
"King of high secure, low latency Internet anonymity](https://www.theguardian."
"com/world/interactive/2013/oct/04/tor-high-secure-internet-anonymity)\" with "
"\"no contenders for the throne in waiting\"."
msgstr ""
"Un extracte d'una valoració secreta de la NSA va caracteritzar Tor com «[el "
"rei de l'anonimat d'Internet d'alta seguretat i baixa latència](https://www."
"theguardian.com/world/interactive/2013/oct/04/tor-high-secure-internet-"
"anonymity)» amb «cap aspirant al tron a l'espera»."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"relationship\">Relationship between Tor and Tails</h1>\n"
msgstr "<h1 id=\"relationship\">Relació entre Tor i Tails</h1>\n"

#. type: Plain text
msgid ""
"- The Tor software is made by [The Tor Project](https://torproject.org/)."
msgstr "- El programari Tor el fa [The Tor Project](https://torproject.org/)."

#. type: Plain text
msgid "- The Tor network is run by a worldwide community of volunteers."
msgstr "- La xarxa Tor està dirigida per una comunitat mundial de voluntaris."

#. type: Plain text
msgid "- Tails is a separate project made by a different group of people."
msgstr ""
"- Tails és un projecte independent fet per un grup diferent de persones."

#. type: Plain text
#, no-wrap
msgid ""
"Tails is a complete operating system which uses Tor as its default networking\n"
"application. The Tor Project recommends the usage of Tails for the use cases\n"
"that are not covered by its own projects (for example the <span\n"
"class=\"application\">Tor Browser</span>).\n"
msgstr ""
"Tails és un sistema operatiu complet que utilitza Tor com a aplicació de xarxa\n"
"predeterminada. El projecte Tor recomana l'ús de Tails per als casos d'ús\n"
"que no estan coberts pels seus propis projectes (per exemple, el <span\n"
"class=\"application\">Navegador Tor</span>).\n"

#. type: Plain text
msgid ""
"But many people use Tor outside of Tails, and many people use Tails to do "
"other things than accessing the Internet through Tor, for example to work "
"offline on sensitive documents."
msgstr ""
"Però moltes persones utilitzen Tor fora de Tails, i moltes persones "
"utilitzen Tails per fer altres coses que no siguin accedir a Internet "
"mitjançant Tor, per exemple per treballar fora de línia en documents "
"sensibles."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"i2p\">About I2P</h1>\n"
msgstr "<h1 id=\"i2p\">Quant a I2P</h1>\n"

#. type: Plain text
msgid ""
"[I2P](https://geti2p.net/) is another anonymity network and alternative to "
"Tor.  Tails used to include I2P until 2017. We removed I2P in [[Tails 2.12|"
"news/version_2.11]] because it was too costly to maintain."
msgstr ""
"[I2P](https://geti2p.net/) és una altra xarxa d'anonimat i alternativa a "
"Tor. Tails solia incloure I2P fins al 2017. Vam eliminar I2P a [[Tails 2.12|"
"news/version_2.11]] perquè era massa costós de mantenir."

#. type: Plain text
msgid ""
"We have no plans to add I2P back in Tails because I2P hasn't yet become "
"popular and usable enough to matter to our [[target audience|contribute/"
"personas]]."
msgstr ""
"No tenim previst tornar a afegir I2P a Tails perquè I2P encara no s'ha fet "
"prou popular i utilitzable com per importar al nostre [[públic objectiu|"
"contribute/personas]]."

#, no-wrap
#~ msgid "<a id=\"relationship\"></a>\n"
#~ msgstr "<a id=\"relationship\"></a>\n"
