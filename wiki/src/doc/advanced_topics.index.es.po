# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-04-22 10:02+0200\n"
"PO-Revision-Date: 2024-04-24 15:36+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"advanced_topicsindex/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
msgid "- [[Running Tails in a virtual machine|advanced_topics/virtualization]]"
msgstr ""
"- [[Arrancar Tails en una máquina virtual|advanced_topics/virtualization]]"

#. type: Bullet: '  * '
msgid "[[*GNOME Boxes*|advanced_topics/virtualization/boxes]]"
msgstr "[[*Cajas de GNOME*|advanced_topics/virtualization/boxes]]"

#. type: Bullet: '  * '
#, fuzzy
#| msgid ""
#| "[[!traillink Accessing_the_internal_hard_disk|advanced_topics/"
#| "internal_hard_disk]]"
msgid "[[*virt-manager*|advanced_topics/virtualization/virt-manager]]"
msgstr ""
"[[!traillink Acceder_al_disco_duro_interno|advanced_topics/"
"internal_hard_disk]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[!traillink Adding_boot_options_using_the_Boot_Loader|advanced_topics/"
#| "boot_options]]"
msgid ""
"- [[Modifying the boot options using the Boot Loader|advanced_topics/"
"boot_options]]"
msgstr ""
"[[!traillink Añadir_opciones_de_arranque_con_el_Boot_Loader|advanced_topics/"
"boot_options]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[!traillink Accessing_the_internal_hard_disk|advanced_topics/"
#| "internal_hard_disk]]"
msgid ""
"- [[Starting Tails from an external hard disk or problematic USB stick|"
"advanced_topics/external_hard_disk]]"
msgstr ""
"[[!traillink Acceder_al_disco_duro_interno|advanced_topics/"
"internal_hard_disk]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[!traillink Accessing_the_internal_hard_disk|advanced_topics/"
#| "internal_hard_disk]]"
msgid ""
"- [[Accessing the internal hard disk|advanced_topics/internal_hard_disk]]"
msgstr ""
"[[!traillink Acceder_al_disco_duro_interno|advanced_topics/"
"internal_hard_disk]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[!traillink Protection_against_cold_boot_attacks|advanced_topics/"
#| "cold_boot_attacks]]"
msgid ""
"- [[Protection against cold boot attacks|advanced_topics/cold_boot_attacks]]"
msgstr ""
"[[!traillink Protección_contra_ataques_cold_boot|advanced_topics/"
"cold_boot_attacks]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[!traillink Accessing_the_internal_hard_disk|advanced_topics/"
#| "internal_hard_disk]]"
msgid "- [[Accessing resources on the local network|advanced_topics/lan]]"
msgstr ""
"[[!traillink Acceder_al_disco_duro_interno|advanced_topics/"
"internal_hard_disk]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[!traillink Accessing_the_internal_hard_disk|advanced_topics/"
#| "internal_hard_disk]]"
msgid "- [[Enabling a wireless device|advanced_topics/wireless_devices]]"
msgstr ""
"[[!traillink Acceder_al_disco_duro_interno|advanced_topics/"
"internal_hard_disk]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[!traillink Accessing_the_internal_hard_disk|advanced_topics/"
#| "internal_hard_disk]]"
msgid "- [[Chatting with *Dino* and OMEMO|advanced_topics/dino]]"
msgstr ""
"[[!traillink Acceder_al_disco_duro_interno|advanced_topics/"
"internal_hard_disk]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[!traillink Accessing_the_internal_hard_disk|advanced_topics/"
#| "internal_hard_disk]]"
msgid "- [[Exchanging Monero with *Feather*|advanced_topics/feather]]"
msgstr ""
"[[!traillink Acceder_al_disco_duro_interno|advanced_topics/"
"internal_hard_disk]]"

#~ msgid "[[*VirtualBox*|advanced_topics/virtualization/virtualbox]]"
#~ msgstr "[[*VirtualBox*|advanced_topics/virtualization/virtualbox]]"

#, fuzzy, no-wrap
#~| msgid ""
#~| "  - [[!traillink Running_Tails_in_a_virtual_machine|advanced_topics/virtualization]]\n"
#~| "    - [[!traillink <span_class=\"application\">VirtualBox</span>|advanced_topics/virtualization/virtualbox]]\n"
#~| "    - [[!traillink <span_class=\"application\">GNOME_Boxes</span>|advanced_topics/virtualization/boxes]]\n"
#~| "    - [[!traillink <span_class=\"application\">virt-manager</span>|advanced_topics/virtualization/virt-manager]]\n"
#~| "  - Persistent Storage\n"
#~| "    - [[!traillink Changing_the_passphrase_of_the_Persistent_Storage|advanced_topics/persistence/change_passphrase]]\n"
#~| "    - [[!traillink Checking_the_file_system_of_the_Persistent_Storage|advanced_topics/persistence/check_file_system]]\n"
#~| "  - [[!traillink Accessing_resources_on_the_local_network|advanced_topics/lan]]\n"
#~| "  - [[!traillink Enabling_a_wireless_device|advanced_topics/wireless_devices]]\n"
#~ msgid ""
#~ "  - [[!traillink Running_Tails_in_a_virtual_machine|advanced_topics/virtualization]]\n"
#~ "    - [[!traillink <span_class=\"application\">VirtualBox</span>|advanced_topics/virtualization/virtualbox]]\n"
#~ "    - [[!traillink <span_class=\"application\">GNOME_Boxes</span>|advanced_topics/virtualization/boxes]]\n"
#~ "    - [[!traillink <span_class=\"application\">virt-manager</span>|advanced_topics/virtualization/virt-manager]]\n"
#~ "  - [[!traillink Modifying_the_boot_options_using_the_Boot_Loader|advanced_topics/boot_options]]\n"
#~ "  - [[!traillink Starting_Tails_from_an_external_hard_disk|advanced_topics/external_hard_disk]]\n"
#~ "  - [[!traillink Accessing_the_internal_hard_disk|advanced_topics/internal_hard_disk]]\n"
#~ "  - Persistent Storage\n"
#~ "    - [[!traillink Changing_the_passphrase_of_the_Persistent_Storage|advanced_topics/persistence/change_passphrase]]\n"
#~ "    - [[!traillink Checking_the_file_system_of_the_Persistent_Storage|advanced_topics/persistence/check_file_system]]\n"
#~ "  - [[!traillink Protection_against_cold_boot_attacks|advanced_topics/cold_boot_attacks]]\n"
#~ "  - [[!traillink Accessing_resources_on_the_local_network|advanced_topics/lan]]\n"
#~ "  - [[!traillink Enabling_a_wireless_device|advanced_topics/wireless_devices]]\n"
#~ "  - [[!traillink Chatting_with_*Dino*_and_OMEMO|advanced_topics/dino]]\n"
#~ "  - [[!traillink Exchanging_Monero_with_*Feather*|advanced_topics/feather]]\n"
#~ msgstr ""
#~ "  - [[!traillink Arrancar_Tails_en_una_máquina_virtual|advanced_topics/virtualization]]\n"
#~ "    - [[!traillink <span_class=\"application\">VirtualBox</span>|advanced_topics/virtualization/virtualbox]]\n"
#~ "    - [[!traillink <span_class=\"application\">GNOME_Boxes</span>|advanced_topics/virtualization/boxes]]\n"
#~ "    - [[!traillink <span_class=\"application\">virt-manager</span>|advanced_topics/virtualization/virt-manager]]\n"
#~ "- Almacenamiento Persistente\n"
#~ "    - [[!traillink Cambiar_la_frase_contraseña_del_Almacenamiento_Persistente|advanced_topics/persistence/change_passphrase]]\n"
#~ "    - [[!traillink Chequear_el_sistema_del_Almacenamiento_Persistente|advanced_topics/persistence/check_file_system]]\n"
#~ "  - [[!traillink Acceder_a_recursos_de_la_red_local|advanced_topics/lan]]\n"
#~ "  - [[!traillink Habilitar_un_dispositivo_inalámbrico|advanced_topics/wireless_devices]]\n"
